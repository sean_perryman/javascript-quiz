/**
 * Created by Sean.Perryman on 4/1/2014.
 */
var questionsArray;
$.getJSON('questions.json').done(function(data) {
    questionsArray = data.questions;
});

//Player variables
var playerAnswers = new Array();
var loggedInPlayer = "none";

//Elemental Variables
///var startButton = document.getElementById("start");
var questions = document.getElementById("questions");
var question = document.getElementById("question");
var answer1Label = document.getElementById("answer1Label");
var answer2Label = document.getElementById("answer2Label");
var answer3Label = document.getElementById("answer3Label");
var answer4Label = document.getElementById("answer4Label");
var radios = document.getElementsByName("answer");

//Set container to screen height
function resize() {
    var viewportheight;

    //The more standards compliant browsers (mozilla/netscape/opera/IE7) use window.innerWidth and window.innerHeight
    if (typeof window.innerWidth != 'undefined') {
        viewportheight = window.innerHeight
    }

    //IE6 in standards compliant mode (i.e. with a valid doctype as the first line in the document)
    else if (typeof document.documentElement != 'undefined' && typeof document.documentElement.clientWidth != 'undefined' && document.documentElement.clientWidth != 0) {
        viewportheight = document.documentElement.clientHeight
    }

    //Older versions of IE
    else {
        viewportheight = document.getElementsByTagName('body')[0].clientHeight
    }
    $(".container").css('height', (viewportheight - 30) +"px");
};

//Current question variable
var currentQuestion = 0;

//Function to set all the quick text on the page
function setQuizText() {
    question.innerHTML = questionsArray[currentQuestion]['question'];
    answer1Label.innerHTML = questionsArray[currentQuestion]['choices'][0];
    answer2Label.innerHTML = questionsArray[currentQuestion]['choices'][1];
    answer3Label.innerHTML = questionsArray[currentQuestion]['choices'][2];
    answer4Label.innerHTML = questionsArray[currentQuestion]['choices'][3];
};

//Start button click listener
$("#start").click(function() {
    $(start).fadeOut("slow", function() {
        //Hide login button
        $("#showLoginForm").hide();

        //Set quiz text
        setQuizText();

        //Fade in questions
        $(questions).fadeIn("slow");
    });
});

//Next button click listener
$("#next").click(function() {
    var itemChecked = false;
    for (var i = 0, length = radios.length; i < length; i++) {
        if (radios[i].checked) {
            playerAnswers.push(i);
            currentQuestion++;
            itemChecked = true;
            if (currentQuestion <= questionsArray.length - 1) {
                $(questions).fadeOut("slow", function() {
                    radios[i].checked = false;
                    setQuizText();
                });
                $(questions).fadeIn("slow");
            } else {
                //Remove the questions
                $(questions).fadeOut("slow", function() {
                    //Display the player score and the reset button
                    var finalScore = 0;

                    for (var i=0; i < questionsArray.length; i++) {
                        if (playerAnswers[i] == questionsArray[i]['correctAnswer']) {
                            finalScore++;
                        } else {
                            alert("Player Answer: " + playerAnswers[i] + " Correct Answer: " + questionsArray[i]['correctAnswer']);
                        }
                    }
                    $("#finalScore").text("Your final score is: " + finalScore);
                    $("#finalScore").show();
                });
            }
            break;
        }
    }
    if (!itemChecked) { alert("You must select one of the answers to move forward."); }
});

//Previous button click listener
$("#previous").click(function() {
    if (currentQuestion > 0) {
        currentQuestion--;
        setQuizText();
        radios[playerAnswers[currentQuestion]].checked = true;
    } else {
        alert("You are at the first question.");
    }
});

// Function to show/hide the login form
function showLoginForm() {
    $("#overlay").fadeToggle("slow", function() {
        $("#username").focus();
    });
};

function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i=0; i<ca.length; i++)
    {
        var c = ca[i].trim();
        if (c.indexOf(name)==0) return c.substring(name.length,c.length);
    }
    return "";
}

// Action to take when the Login button inside the form is clicked
$("#login").click(function() {
    var username = document.getElementById("username");
    var password = document.getElementById("password");

    if (getCookie("username")) {
        //Get cookie
        var cookieData = getCookie("username");

        //Pull stored credentials from HTML5 local storage
        var storedUsername = localStorage.getItem("username");
        var storedPassword = localStorage.getItem("password");
    } else {
        //Set cookie
        document.cookie = "username=" + username.value;

        //Write username/password to HTML5 local storage
        localStorage.setItem("username", username.value);
        localStorage.setItem("password", password.value);
    }

    function logInUser() {
        loggedInPlayer = username.value;
        showLoginForm();
        $("#showLoginForm").hide();
        $("#loggedInUser").text("Logged in as " + loggedInPlayer);
        $("#loggedInUser").fadeIn("slow");
        $("#logout").fadeIn("slow");
    }

    if (!cookieData) {
        alert("successfully logged in");
        logInUser();
    } else if (username.value == storedUsername && password.value == storedPassword) { //Change this to check HTML5 local storage
        alert("Username and password match!");
        logInUser();
    } else {
        alert("Invalid username or password. Please try again.");
    }
});

//Logout button on the main page
$("#logout").click(function() {
    alert("If I was a finished application, I would be logging out here.");
    loggedInPlayer = "none";
    $("#logout").fadeOut("slow", function() {
        $("#loggedInUser").fadeOut("slow");
        $("#showLoginForm").fadeIn("slow");
    });
});